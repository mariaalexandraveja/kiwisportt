let select = document.getElementById("select");
let list = document.getElementById("list");
let selectText = document.getElementById("selectText");
let options = document.getElementsByClassName("options");
let inputField = document.getElementById("inputField");
let elegido = document.querySelector(".elegido");


select.onclick = function () {
    list.classList.toggle("open");
}

for (option of options) {
    option.onclick = function () {
        selectText.innerHTML = this.innerHTML;
        let texto = selectText.innerHTML;
        if(texto == 'ID' || texto == 'Cedula del Cliente') {
            inputField.setAttribute("type", "number");
        }
        else {
            inputField.setAttribute("type", "text");
        }
        
        elegido.innerHTML = `<input class="form-control" type="text" placeholder="Hidden input" id="eleccion" name="eleccion" value="${texto.replaceAll(' ', '_')}" hidden="true">`
        if(texto == 'Fecha de Inicio') {
            inputField.placeholder = "Ingresar formato YYYY-MM-DD";
        }
        else {
            inputField.placeholder = "Buscar en " + selectText.innerHTML;
        }
    }
}
