// Cargar tabla en vista agregar pedido
const botonAgregar = document.querySelector(".agregarProducto");

if(botonAgregar != null) {
    let numeroProducto = 0;
    botonAgregar.addEventListener("click", async () => {
        numeroProducto++;
        const productos = document.querySelector(".outputProducto");
        const fragmento = document.createDocumentFragment();
        const div = document.createElement("DIV");
        const titulo = document.createElement("DIV");
        titulo.classList.add("pt-2", "pb-2");
        titulo.innerHTML = `<h4>Producto #${numeroProducto}</h4>`;
        div.classList.add("container", "p-0", "m-0");
        div.innerHTML = `
                    <div class="row fila_${numeroProducto}">
                        <div class="col-12 col-md-6 col-lg-4 pt-2 pb-2">
                            <div class="form-outline">
                                <label class="form-label" for="form3Examplev4">Tipo del producto</label>
                                <select class="form-select" name="tipo_producto#${numeroProducto}"
                                    aria-label="Default select example">
                                    <option value="camisa">Camisa</option>
                                    <option value="polo">Polo</option>
                                    <option value="buzo">Buzo</option>
                                    <option value="pantaloneta">Pantaloneta</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 pt-2 pb-2">
                            <div class="form-outline">
                                <label class="form-label" for="form3Examplev4">Sexo:</label>
                                <select class="form-select" name="sexo#${numeroProducto}" aria-label="Default select example">
                                    <option selected value="femenino">Femenino</option>
                                    <option value="masculino">Masculino</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 pt-2 pb-2">
                            <div class="form-outline">
                                <label class="form-label" for="form3Examplev4">Color:</label>
                                <input type="text" id="color" name="color#${numeroProducto}" class="fs-6 form-control form-control-lg"
                                    required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-4 pt-2 pb-2">
                            <div class="form-outline">
                                <label class="form-label" for="form3Examplev4">El producto presenta
                                    sublimado:</label>
                                <select class="form-select" name="sublimado#${numeroProducto}" aria-label="Default select example">
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 pt-2 pb-2">
                            <div class="form-outline">
                                <label class="form-label" for="form3Examplev4">El producto presenta
                                    bordado:</label>
                                <select class="form-select" name="bordado#${numeroProducto}" aria-label="Default select example">
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 pt-2 pb-2">
                            <div class="form-outline">
                                <label class="form-label" for="form3Examplev4">El producto presenta
                                    estampado:</label>
                                <select class="form-select" name="estampado#${numeroProducto}" aria-label="Default select example">
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        
                        <div class="col-12 col-md-6 pt-2 pb-2">
                            <div class="form-outline">
                                <label class="form-label" for="form3Examplev4">Cantidad del
                                    producto:</label>
                                <input type="number" id="cantidad#${numeroProducto}" name="cantidad#${numeroProducto}"
                                    class="form-control form-control-lg" required>
                            </div>
                        </div>

                        <div class="col-12 col-md-6 pt-2 pb-2">
                            <div class="form-outline">
                                <label class="form-label" for="form3Examplev4">Costo:</label>
                                <input type="number" id="costo#${numeroProducto}" name="costo#${numeroProducto}"
                                    class="form-control form-control-lg" required>
                            </div>
                        </div>
                        <div class="col-12 pt-2 pb-2">
                            <div class="form-outline">
                                <label class="form-label" for="form3Examplev4">Descripción:</label>
                                <textarea type="text" id="descripcion#${numeroProducto}" name="descripcion#${numeroProducto}"
                                    class="form-control form-control-lg" required> </textarea>
                            </div>
                        </div>
                    </div>
        `;
        fragmento.appendChild(titulo);
        fragmento.appendChild(div);
        productos.appendChild(fragmento);
    });
}
