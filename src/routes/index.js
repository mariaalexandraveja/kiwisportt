const express = require('express');
const router = express.Router();
const { isLoggedIn } = require('../lib/helpers');
const query = require('../lib/queries');

router.get('/welcome', isLoggedIn, async (req, res) => {
    let pedidos;
    if(req.user.tipo == 0)
        pedidos = await query.consultarPedidosPorUsuario(req.user.correo);
    else
        pedidos = await query.consultarPedidos();
    res.render('home/welcome', { pedidos });
});

router.post('/welcome', isLoggedIn, async (req, res) => {
    let cliente = {}, pedido = {}, productos = [], aux = {};
    let i = 0, j = 0, sum = 0;
    for(elemento in req.body) {
        if(i < 4) {
            cliente[elemento] = req.body[elemento];
        }
        else if (i < 8) {
            pedido[elemento] = req.body[elemento];
        }
        else {
            aux[elemento] = req.body[elemento];
            if(elemento.includes('costo#')) {
                sum += parseInt(req.body[elemento]);
            }
            j++;
        }
        if(j > 8) {
            productos.push(aux);
            aux = {};
            j = 0;
        }
        i++;
    }
    cliente['usuario'] = req.user.correo;
    pedido['id_cliente'] = cliente['id_cliente'];
    pedido['nombre_equipo'] = cliente['nombre_equipo'];
    pedido['usuario'] = req.user.correo;
    pedido['costo'] = sum;
    const queryCliente = await query.consultarCliente(cliente);
    if(queryCliente.length <= 0) {
        const addCliente = await query.addCliente(cliente);
    }
    const addPedido = await query.addPedido(pedido);
    const addProductos = await query.addProductos(productos, addPedido['insertId']);
    const pedidos = await query.consultarPedidosPorUsuario(req.user.correo);
    res.redirect('/welcome');
});

router.get('/registrar', isLoggedIn, async (req, res) => {
    res.render('pedidos/addPedido');
});

router.get('/consultar', isLoggedIn, async (req, res) => {
    res.render('pedidos/consultarPedido');
});

router.post('/consultar', isLoggedIn, async (req, res) => {
    let pedidos = null;
    if(req.body['eleccion'] == 'ID') {
        if(req.user.tipo == 0)
            pedidos = await query.consultarPedidosPorID_U(req.body['campo'], req.user.correo);
        else 
            pedidos = await query.consultarPedidosPorID(req.body['campo']);
    } 
    else if(req.body['eleccion'] == 'Cedula_del_Cliente') {
        if(req.user.tipo == 0)
            pedidos = await query.consultarPedidosPorCliente_U(req.body['campo'], req.user.correo);
        else 
            pedidos = await query.consultarPedidosPorCliente(req.body['campo']);
    }
    else if(req.body['eleccion'] == 'Nombre_del_Equipo') {
        if(req.user.tipo == 0)
            pedidos = await query.consultarPedidosPorNombreEquipo_U(req.body['campo'], req.user.correo);
        else 
            pedidos = await query.consultarPedidosPorNombreEquipo(req.body['campo']);
    }
    else if(req.body['eleccion'] == 'Fecha_de_inicio'){
        if(req.user.tipo == 0)
            pedidos = await query.consultarPedidosPorFechaInicio_U(req.body['campo'], req.user.correo);
        else 
            pedidos = await query.consultarPedidosPorFechaInicio(req.body['campo']);
    }
    else {
        if(req.user.tipo == 0) 
            pedidos = await query.consultarPedidosIniciados_U(req.user.correo);
        else 
            pedidos = await query.consultarPedidos();
    }
    res.render('pedidos/consultarPedido', { pedidos });
});


router.post('/verPedido', isLoggedIn, async (req, res) => {
    const pedido = await query.consultarPedidosPorID(req.body['id_pedido'], req.user.correo);
    const producto = await query.consultarProductos(req.body['id_pedido']);
    res.render('pedidos/verPedidos', { pedido, producto });
});

router.post('/eliminar', isLoggedIn, async (req, res) => {
    const eliminar = await query.eliminarPedido(req.body['id_pedido']);
    res.render('pedidos/consultarPedido');
});

router.post('/actualizar', isLoggedIn, async (req, res) => {
    const actualizar = await query.actualizarPedido(req.body['id_pedido']);
    res.render('pedidos/consultarPedido');
});

router.get('/administrar', isLoggedIn, async (req, res) => {
    const usuarios = await query.consultarUsuarios();
    console.log(usuarios);
    res.render('usuarios/verUsuario', { usuarios });
});


module.exports = router;