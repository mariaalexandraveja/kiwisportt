const express = require('express');
const morgan = require('morgan');
const { engine } = require('express-handlebars');
const path = require('path');
const flash = require('connect-flash');
const session = require('express-session');
const MySqlStore = require('express-mysql-session');
const db = require('./keys');
const passport = require('passport');
const handlebars = require('handlebars');

// inicializar
const app = express();
require('./lib/passport');
// Ajustes

app.set('port', process.env.PORT || 4000);
app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', engine({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs',
    helpers: require('./lib/handlebars')
  }))
  app.set('view engine', '.hbs');

// Middlewares
handlebars.registerHelper('dateFormat', require('handlebars-dateformat'));
app.use(session({
    secret: 'sesion',
    resave: false,
    saveUninitialized: false,
    store: new MySqlStore(database)

}));
app.use(flash());
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false})); //Para que solo envien datos sencillos, no como imagenes
app.use(express.json());

app.use(passport.initialize());
app.use(passport.session());

// Global Variables
app.use((req, res, next) => {
    app.locals.success = req.flash('success');
    app.locals.failure = req.flash('failure');
    app.locals.user = req.user;
    next();
});

// Routes
app.use(require('./routes/index'));
app.use(require('./routes/authentication'));

// Public
app.use(express.static(path.join(__dirname, 'public')))
// Start

app.listen(app.get('port'), () => {
    console.log('Server en el puerto', app.get('port'));
});