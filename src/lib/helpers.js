const bcrypt = require('bcryptjs');

const helpers = {};

helpers.encriptar = async (password) => {
    //Cadena aleatoria, a mayor número más segura, pero más lenta de generar
    const salt = await bcrypt.genSalt(10); 
    // Contraseña cifrada
    const hash = bcrypt.hash(password, salt);
    return hash;
};

helpers.compararContrasenia = async (password, savedPassword) => {
    try {
        const result = await bcrypt.compare(password, savedPassword);
        return result;
    }
    catch (e) {
        console.log(e);
    }
};

helpers.isLoggedIn = (req, res, next) => {
    if(req.isAuthenticated()) return next();
    return res.redirect('/signin');
};

helpers.isLoggedOut = (req, res, next) => {
    if(!req.isAuthenticated()) return next();
    return res.redirect('/welcome');
};

module.exports = helpers;